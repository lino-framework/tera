# -*- coding: utf-8 -*-
# fmt: off
import datetime

from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(globals())

extensions += ['lino.sphinxcontrib.logo']
# from rstgen.sphinxconf import interproject
# interproject.configure(globals(), 'atelier')

project = "Lino Tera"
copyright = '2014-{} Rumma & Ko Ltd'.format(datetime.date.today().year)
html_title = "Lino Tera"
# html_context.update(public_url='https://tera.lino-framework.org')
# suppress_warnings = ['image.nonlocal_uri']

# from rstgen.sphinxconf import interproject
#
# interproject.configure(
#     globals(),
#     'atelier',
#     # cg=('https://community.lino-framework.org/', None),
#     ug=('https://using.lino-framework.org/', None))
