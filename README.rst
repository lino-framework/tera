=========================
The ``lino-tera`` package
=========================




Lino Tera is a customizable management system for therapeutic centres.

- Project homepage: https://gitlab.com/lino-framework/tera

- Documentation:
  https://lino-framework.gitlab.io/tera/

- Functional specification see
  https://www.lino-framework.org/specs/tera

- There is also a User's Manual in German: `online using Sphinx
  <https://tera.lino-framework.org/de>`__ (work in progress) and `PDF
  using LibreOffice
  <https://gitlab.com/lino-framework/tera/raw/master/docs/dl/Handbuch_Lino_Tera.pdf>`__
  (not maintained).

- For *introductions* and *commercial information* about Lino Tera
  please see `www.saffre-rumma.net
  <https://www.saffre-rumma.net>`__.




