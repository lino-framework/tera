��    g      T  �   �      �     �     �     �     �     �     �     �     	  
   	     	     3	     @	     T	  
   b	  
   m	     x	     	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
      
  	   .
     8
     F
     T
  	   d
     n
     w
     �
     �
     �
     �
     �
     �
     �
     �
     �
          -     =     P     a  	   m  
   w     �  	   �     �     �  "   �     �     �     �     �       	        (     A     G     T     b     j     v     ~  	   �     �  	   �     �  	   �  
   �     �     �     �  	                  '     /     A     V     ^  	   q  	   {     �     �     �     �     �     �     �     �     �     �  �       �     �  
   �     �     �  
   �  
   �     �     �     �  
             .     <     E     N     U  	   p     z     �     �  
   �     �     �     �     �     �     �     �  	             $     3     D     T     h     z     �     �     �     �     �     �     �                     4     Q     g     }     �     �     �  	   �     �     �     �      �                0     J     X     q     z     �     �     �     �  
   �     �  	   �     �  
   �  
                       #     0     I     Z     b     u     �     �     �     �     �     �  	   �     �     �     �     	       	   )  
   3     >     C  
   X         3   (           >   P   #   W          L   7      !   Q                        I   M         4             Y   d   R   ?               
                 =   \       	   "          <   )   b   _   %                      &   C   D   U   0   1                 a       2       9             .   X   6       ^       *   :   J   F          -   e   G   O      /       N   `       B   T       '      H   ,                 K   5   A   c   [   @   f                  8   $      g   S         +   ]   ;      V   E   Z       Active Add a colleague Adults Amount perceived Appointments Attendee By calendar event By presence Called off Career interruption Cash daybook Cash daybook Daniel Cash daybooks Children M Children P Client Detailed description Division Divisions Dossier Dossier type Dossier types Dossiers Employed Ending reason Familiar reasons Fee Fee categories Fee category Fees First contact Forwarded Group meeting Group therapy Health problems Homemaker Inactive Income category Independent Individual appointment Individual therapies Individual therapy Insert [Ctrl+S] Inside Interpreter Invalid Invoice recipient Invoice recipient in dossiers Invoiceable fee Invoicing policies Invoicing policy Life groups Life mode Life modes Manager Mandatory Missed Missing motivation More than one participant below 18 My cash roll New note No cash daybook defined No fee No participant below 18 Note type One participant below 18 Other Other groups Our reference Outside Participant Patient Patients Perceived Procurer Procurers Professional situation Residence Residences Retired Return to home country Sales on therapies Scheduled Service type Service types Session Short description Social aid recipient Student Successfully ended Therapies Therapist Therapy Therapy domain Total amount Translator type Translator types Unknown Workless for {product} on {dates} {} appointments Project-Id-Version: lino-noi 0.0.2
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2024-11-16 19:08+0200
Last-Translator: Luc Saffre <luc.saffre@gmail.com>
Language-Team: de <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.16.0
X-Generator: Poedit 3.0.1
 Aktiv Kollegen hinzufügen Erwachsene Kassierter Betrag Termine Teilnehmer Pro Termin Pro Anwesenheit Abgesagt Laufbahnunterbrechung Kassenbuch Kassenbuch Daniel Kassenbücher Kinder M Kinder P Klient Ausführliche BEschreibung Abteilung Abteilungen Akte Aktenart Aktenarten Akten Arbeiter/Angestellter Beendigungsgrund Familiäre Gründe Gebühr Gebührenkategorien Gebührenkategorie Gebühren Erstkontakt Weitergeleitet Gruppengespräch Gruppentherapie Gesundheitsprobleme Hausfrau/Hausmann Inaktiv Einkommenskategorie Selbstständig/Freiberufler Einzelgespräch Einzeltherapien Einzeltherapie Einfügen [Ctrl+S] Innerhalb der DG Dolmetscher Berufsunfähig Rechnungsempfänger Rechnungsempfänger in Akten Fakturierbare Gebühr Fakturierungsmethoden Fakturierungsmethode Lebensgruppen Lebensweise Lebensweise Verwalter Verpflichtend Verpasst Fehlende Motivation Mehr als ein Teilnehmer unter 18 Meine Kassenrolle Notiz erstellen Kein Kassenbuch definiert Keine Gebühr Kein Teilnehmer unter 18 Notizart Ein Teilnehmer unter 18 Andere Therapeutische Gruppen Unser Zeichen Außerhalb der DG Teilnehmer Patient Patienten Kassiert Vermittler Vermittler Beruf Wohnort Wohnorte Im Ruhestand Rückkehr ins Heimatland Umsatz Therapien Geplant Dienstleistungsart Dienstleistungsarten Sitzung Kurzbeschreibung Sozialhilfeempfänger In Ausbildung Erfolgreich beendet Akten Therapeut Therapie Bereich Gesamtbetrag Übersetzung Übersetzungsarten Unbekannt Arbeitslos für {product} am {dates} {} Termine 