# -*- coding: UTF-8 -*-
# Copyright 2017 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import _

from lino_xl.lib.coachings.ui import *


class CoachingDetail(dd.DetailLayout):

    # main = 'general address more sales accounting'
    main = 'general address more accounting'

    # general = dd.Panel(PartnerDetail.main,label=_("General"))

    general = dd.Panel("""
    overview:30 contact_box:30 lists.MembersByPartner:20
    bottom_box
    """,
                       label=_("General"))

    address = dd.Panel("""
    address_box
    sepa.AccountsByPartner
    """,
                       label=_("Address"))

    more = dd.Panel("""
    id language
    addr1 url
    #courses.CoursesByCompany
    # changes.ChangesByMaster
    excerpts.ExcerptsByOwner cal.GuestsByPartner
    """,
                    label=_("More"))

    ledger_a = """
    salesrule__invoice_recipient vat_regime
    payment_term salesrule__paper_type
    """

    # sales = dd.Panel("""
    # """, label=dd.plugins.trading.verbose_name)

    bottom_box = """
    remarks:50 checkdata.MessagesByOwner:30
    """

    # A layout for use in Belgium
    address_box = """
    name
    street:25 #street_no street_box
    addr2
    country zip_code:10 city
    """

    contact_box = """
    #mti_navigator
    email
    phone
    #fax
    gsm
    """

    accounting = dd.Panel("""
    ledger_a accounting.MovementsByPartner
    trading.InvoicesByPartner
    """,
                      label=dd.plugins.accounting.verbose_name)


Coachings.set_detail_layout(CoachingDetail())
