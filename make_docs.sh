#!/bin/bash
set -e
SHARED=../book/docs/shared
if [ -d $SHARED ] ; then
  cp -au $SHARED docs/
fi
